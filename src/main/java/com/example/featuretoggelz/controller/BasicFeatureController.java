package com.example.featuretoggelz.controller;

import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.featuretoggelz.aop.CustomFeatureEnablerAnnotation;
import com.example.featuretoggelz.config.ConfigProperties;

@RestController
public class BasicFeatureController {
	
	@Autowired
	 private ConfigProperties properties;
	 
	@GetMapping("/")
	public String getbasicfetureValue() {

		String str = ((BasicFeatureController) AopContext.currentProxy()).theOtherMethod();
		return "Hello " + str;
	}
	
	@GetMapping("/ad")
	@CustomFeatureEnablerAnnotation(value = "advancefeature",fallback = HttpStatus.HTTP_VERSION_NOT_SUPPORTED)
	public String getadvancefetureValue() {
		return "Hello from advance" ;
	}

	@CustomFeatureEnablerAnnotation(value = "basicfeature" )
	public String theOtherMethod(){
		return "Suvankar";
	}

}
