package com.example.featuretoggelz.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.featuretoggelz.config.ConfigProperties;

@RestController
public class ConfigurtionController {
	
	@Autowired
	 private ConfigProperties properties;
	 
	    @GetMapping("/feature-flags")
	    public Map<String,Boolean> getProperties() {
	    	Map<String,Boolean> featureValue = new HashMap<>();
	    	featureValue.putAll( properties.getFeature());
	  
	    	return featureValue;
	    }

}
