package com.example.featuretoggelz.aop;

import com.example.featuretoggelz.config.ConfigProperties;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Aspect
@Component
@EnableAspectJAutoProxy(exposeProxy = true)
public class FeatureAspect {

    @Autowired
    private ConfigProperties properties;

    // @Around("@annotation(com.example.featuretoggelz.aop.CustomFeatureEnablerAnnotation)")
    @Around(value = "@annotation(customFeatureEnablerAnnotation)")
    public Object loadFeature(ProceedingJoinPoint joinPoint, CustomFeatureEnablerAnnotation customFeatureEnablerAnnotation) throws Throwable {
        final long start = System.currentTimeMillis();

        System.out.printf("Hi");
        if (!properties.getFeature().get(customFeatureEnablerAnnotation.value())) {
            if (customFeatureEnablerAnnotation.fallback() != null &&
                    customFeatureEnablerAnnotation.fallback() != HttpStatus.ACCEPTED) {
                throw new ResponseStatusException(HttpStatus.NOT_IMPLEMENTED);
            } else {
                return null;
            }

            //return null;
        }
        final Object proceed = joinPoint.proceed();


        final long executionTime = System.currentTimeMillis() - start;
        System.out.println(customFeatureEnablerAnnotation.value());


        System.out.println(joinPoint.getSignature() + " executed in " + executionTime + "ms" + properties.getFeature().get(customFeatureEnablerAnnotation.value()));

        return proceed;
    }
}
