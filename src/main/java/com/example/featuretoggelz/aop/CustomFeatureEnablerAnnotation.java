package com.example.featuretoggelz.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.http.HttpStatus;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CustomFeatureEnablerAnnotation {

	 public String value() default "";
	 public HttpStatus fallback() default HttpStatus.ACCEPTED;
}
