package com.example.featuretoggelz.config;

import java.util.HashMap;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "featurelist")
@RefreshScope
public class ConfigProperties {
	
	private HashMap<String, Boolean> feature;

	public HashMap<String, Boolean> getFeature() {
		return feature;
	}

	public void setFeature(HashMap<String, Boolean> feature) {
		this.feature = feature;
	}
    

}
