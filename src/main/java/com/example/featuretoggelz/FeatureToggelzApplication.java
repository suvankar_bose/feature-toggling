package com.example.featuretoggelz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication

public class FeatureToggelzApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeatureToggelzApplication.class, args);
	}

}
